ROS node for Oculus Rift
========================
This is [ROS](http://ros.org) driver for [ueye stereo camera](https://en.ids-imaging.com/download-ueye-lin64.html).
see https://en.ids-imaging.com/download-ueye-lin64.html for camera drivers.

Install
-----------------
Make sure you have downloaded and installed the camera driver
https://en.ids-imaging.com/download-ueye-lin64.html
Please note that after modifying the parameters in the cfg folder you have to recompile the package to take them effect.
Also the ros packages http://wiki.ros.org/image_common 
and http://wiki.ros.org/image\_pipeline may be required.


Packages
------------------
* stereo: stereo camera node
* camera: mono camera node


Nodes
=============

stereo
------------------
publishes the two video streams of the camera

### publish

* /left/image_raw 
* /right/image_raw 

### param
* see /cfg directory for modifying the parameters


License
-----------
BSD